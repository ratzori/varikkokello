#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 400000UL
#include <util/delay.h>

//PD0  = Siirtorekisterin clk
//PD1  = Siirtorekisterin data
//PA1  = Siirtorekisteri Output enable

//INT0 = Start / stop
//PB0  = V�henn� minuutteja
//PB1  = Lis�� minuutteja

const uint8_t merkki[] =
{0xFC,0x60,0xDA,0xF2,0x66,0xB6,0xBE,0xE0,0xFE,0xF6}; //7-segmentin numerot 0-9

volatile uint8_t sek=0;
volatile uint8_t kymsek=0;
volatile uint8_t kymmin=2;		//kellon kymmenet minuutit
volatile uint8_t min=0;    		//kellon minuutit
volatile uint8_t oikpiste=0;	//7-segmentin piste minuutti-osassa 
volatile uint8_t vaspiste=0;	//--..-- kymmin-osassa
volatile uint8_t jakaja=0;
volatile uint8_t standby_jakaja=0;
volatile uint8_t standby_count=0;
volatile uint8_t standby_tila=0; // 0 = normaalitila, 1=standby
volatile uint8_t start=0;

//volatile uint8_t start_kytkin=1;
//volatile uint8_t start_kytkin_ed=1;

//volatile uint8_t debug=0;

ISR(TIMER0_OVF_vect) { //Timer0 ulkoinen keskeytys
	// (1/32.768kHz) x 256(overflow) x 128) = 1 sek
	
	if(start==1) { //Muutetaan muuttujia vain jos kellon pit�� k�yd�
		jakaja++;
		
		if (jakaja == 64) oikpiste=!oikpiste;
		
		if (jakaja == 128) {
			jakaja = 0;
			oikpiste=!oikpiste;
		
			sek--;
			if (sek==255) {
				sek=9;
				kymsek--;
			}
			if (kymsek==255) {
				kymsek=5;
				laske_alas();
			}
		}
		if(kymmin==0 && min==0 && kymsek==0 && sek==0) {
			start=0;    //siirry asetustilaan
			oikpiste=0;	//7-segmentin piste minuutti-osassa 
			vaspiste=0;	//--..-- kymmin-osassa
			sek=0;
			kymsek=0;
			naytolle();  //p�ivitet��n n�ytt� oikeaksi ennen vilkutusta
			vilkuta();
		}
	}
	
	else {	//Kello ei k�y, joten kasvatetaan standby-laskuria
		
		if (standby_tila == 0) {
			standby_jakaja++;

			if (standby_jakaja==128) {
				standby_jakaja=0;
				standby_count++;  //Kasvatetaan sekunnin v�lein
			}
			if (standby_count==120) { //120s = 2 min
				standby_count=0;
				standby();
			}
		}
	}


}

void standby(void) {
	_delay_ms(500);
	standby_tila = 1;
	PORTA |= (1<<PA1); //Output-enable pois

	while (standby_tila) {

		if (~PINB & (1 << PB0)) {  //PB0 painettu
			standby_tila=0;
  			  }

		if (~PINB & (1 << PB1)) {  //PB1 painettu
			standby_tila=0;
			}

		if (~PIND & (1 << PD2)) { //start-stop painettu
			standby_tila=0;
	}
	_delay_ms(500);

	}

	
}


void vilkuta(void) {
	volatile uint8_t i;
	
 	for(i=0;i<16;i++) {
		PORTA |= (1<<PA1);
		_delay_ms(900);
		PORTA &= ~(1<<PA1);
		_delay_ms(900);
	}
}


void aseta_laskuri(void) {
	if ((~PINB & (1 << PB0)) && (~PINB & (1 << PB1))) {
		_delay_ms(300);
	    }
		else {
			if (~PINB & (1 << PB0)) {  //PB0 painettu
			laske_alas();
			_delay_ms(300);
  			  }

			if (~PINB & (1 << PB1)) {  //PB1 painettu
			laske_ylos();
			_delay_ms(300);
			}
	}
	oikpiste=0;	//7-segmentin piste minuutti-osassa 
	vaspiste=0;	//--..-- kymmin-osassa
	sek=0;
	kymsek=0;
}

void laske_ylos(void) {
    min++;
    if (min==10) {
		min=0;
		kymmin++;
		}
    if (kymmin==10) kymmin=0;
}	

void laske_alas(void) {
  min--;
	if (min==255) {
		min=9;
		kymmin--;
		}
	if (kymmin==255) kymmin=9;	
} 

void naytolle(void) {  //Datan siirto rekistereihin
   volatile uint8_t i;
   volatile uint8_t data;
   volatile uint8_t ykkoset; //7-segmentille muunnetut minuutit
   volatile uint8_t kymmenet;//7-segmentille muunnetut kymmenet minuutit
   
   if (min==0 && kymmin==0 && start==1) {
		ykkoset = merkki[sek];     //Haetaan numeroa vastaava bittijono
		kymmenet = merkki[kymsek]; //---//---
		data = ykkoset;
	}
	else { 
		ykkoset = merkki[min];     //Haetaan numeroa vastaava bittijono
		kymmenet = merkki[kymmin]; //---//---
		data = ykkoset;
		data |= oikpiste; //lis�t��n desimaalipiste siirrett�v��n dataan
	}

   PORTA |= (1<<PA1); //OE->1

   for(i=0;i<8;i++) { //Siirret��n minuutit siirtorekisteriin
    
	PORTD = (((data & 1)<<1) | 1 | 4);  //+1 = output-enable  / +4 = int0
    
		
	PORTD &= ~(1<<PD0); //clk 0 - versio 1.1
	PORTD |= (1<<PD0);   //clk 1

	data = data>>1; 
	}

   data = kymmenet;
 //  data |= vaspiste; //lis�t��n desimaalipiste siirrett�v��n dataan

    for(i=0;i<8;i++) { //Siirret��n kymmenet minuutit siirtorekisteriin
    PORTD = (((data & 1)<<1) | 1 | 4);  //+1 = output-enable  / +4 = int0

	PORTD &= ~(1<<PD0); //clk 0
	PORTD |= (1<<PD0);   //clk 1


	data = data>>1;
	}

	PORTD &= ~(1<<PD0); //clk 0
	PORTD |= (1<<PD0);   //clk 1


	PORTA &= ~(1<<PA1); //OE->0=Numerot n�yt�ille
}
 
int alustus(void) {

	//PD0  = Siirtorekisterin clk
	//PD1  = Siirtorekisterin data
	//PA1  = Siirtorekisteri Output enable
	//INT0 = Start / stop
	//PB0  = V�henn� minuutteja
	//PB1  = Lis�� minuutteja
		
	DDRA = 2; //Sallitaan CLK l�ht�
	DDRB = 0; //PB0 ja PB1 sis��ntulot
    DDRD = 3; //Sallitaan PD1 ja PD2 l�ht� (data ja output enable)
			  //Timer0 sis��n kun DDRD4=0

	PORTB = 3; //PB0 ja PB1 yl�svetovastukset
	PORTD = 5; //INTO yl�svetovastus ja Output enable -> 1 (nolla-aktiivinen)
	//MCUCR |= (1<<ISC01); //Keskeytys kun 1->0
	
	TIMSK |= (1 << TOIE0); //kello k�ytt��n
	TCCR0B = 6; //K�ytet��n ulkoista kelloa T0 pinnille, laskeva reuna

	sei(); //sallitaan keskeytykset

	//cli(); est�� keskeytykset
}

int tilan_valinta(void) {
	if (~PIND & (1 << PD2)) {
		start=!start;
		_delay_ms(500);
	}
}

int main(void) {
	
	alustus(); //Alustetaan toiminnot
		
	while(1) {
		tilan_valinta();
		//start=1;
		if (!start) {
		aseta_laskuri(); //Ei, menn��n laskurin asetustilaan
		}
		naytolle();
		_delay_ms(5);
	}

  return(0);
}


